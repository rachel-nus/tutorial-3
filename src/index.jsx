// Defaults that are to be removed for Tutorial 4
const currentDate = new Date(Date.now());
const waitlist = [
  {
    id: 1,
    customerName: "mickey mouse",
    phoneNumber: 84889687,
    createdTime: currentDate.toUTCString(),
  },
  {
    id: 2,
    customerName: "cheshire cat",
    phoneNumber: 84172637,
    createdTime: currentDate.toUTCString(),
  },
  {
    id: 3,
    customerName: "buzz lightyear",
    phoneNumber: 98472631,
    createdTime: currentDate.toUTCString(),
  },
];

const pinkContainerClassName =
  "w3-container w3-padding-16 w3-pale-red w3-center w3-wide";
const genericButtonClassName =
  "w3-button w3-round w3-red w3-opacity w3-hover-opacity-off";
const deleteButtonClassName =
  "w3-button w3-round w3-grey w3-opacity w3-hover-opacity-off";
class AddCustomer extends React.Component {
  render() {
    return (
      <div className={pinkContainerClassName} id="addCustomerButton">
        <button className={genericButtonClassName}>Add a Customer</button>
      </div>
    );
  }
}

class CustomerForm extends React.Component {
  render() {
    return (
      <div className={pinkContainerClassName}>
        <div className="form-group">
          <label htmlFor="custName">
            <b> Name:</b>
          </label>
          <input type="text" id="custName" name="custName" />
          <br />
          <br />
          <label htmlFor="custPhone">
            <b> Phone Number:</b>
          </label>
          <input type="text" id="custPhone" name="custPhone" />
          <br />
          <br />
          <button className={genericButtonClassName}>
            Add Customer to Waitlist
          </button>
        </div>
      </div>
    );
  }
}

class DeleteCustomer extends React.Component {
  render() {
    return (
      <button className={deleteButtonClassName}>
        <i className="fa fa-close"></i>
      </button>
    );
  }
}

class DisplayCustomers extends React.Component {
  render() {
    return (
      <div className={pinkContainerClassName}>
        <h3>The Waitlist</h3>
        <WaitListTable />
        <button className={genericButtonClassName}>View Waitlist</button>
      </div>
    );
  }
}

class DisplayFreeSlots extends React.Component {
  render() {
    const MAX_WAITING = 25;
    const waitlistNum = waitlist.length;
    return (
      <div className={pinkContainerClassName}>
        Number of Free Slots: {MAX_WAITING - waitlistNum}
      </div>
    );
  }
}

class ClearWaitList extends React.Component {
  render() {
    return (
      <div className={pinkContainerClassName} id="clearWaitListButton">
        <button className={genericButtonClassName}>Clear Waitlist</button>
      </div>
    );
  }
}

class DisplayHomepage extends React.Component {
  render() {
    return (
      <div id="homepage">
        <div className="w3-container w3-padding-16 w3-pale-blue w3-center w3-wide">
          <h1 className="w3-text-grey">
            <b>Hotel California Waitlist System</b>
          </h1>
        </div>
        <br />
        <DisplayFreeSlots />
      </div>
    );
  }
}

class WaitListRow extends React.Component {
  render() {
    const customerProp = this.props.customer;
    return (
      <tr>
        <td>{customerProp.id}</td>
        <td>{customerProp.customerName}</td>
        <td>{customerProp.phoneNumber}</td>
        <td>{customerProp.createdTime}</td>
        <td>
          <DeleteCustomer />
        </td>
      </tr>
    );
  }
}

class WaitListTable extends React.Component {
  render() {
    const waitListRows = waitlist.map((customer) => (
      <WaitListRow key={customer.id} customer={customer} />
    ));
    return (
      <div className={pinkContainerClassName} id="theWaitlist">
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Customer Name</th>
              <th>Phone Number</th>
              <th>Created Time</th>
            </tr>
          </thead>
          <tbody>{waitListRows}</tbody>
        </table>
      </div>
    );
  }
}

class SidebarNav extends React.Component {
  render() {
    return (
      <div id="sidebarNav" className="w3-sidebar w3-light-grey w3-bar-block">
        <h3 className="w3-bar-item">
          <b>☰ Menu</b>
        </h3>
        <a href="#homepage" className="w3-bar-item w3-button">
          Homepage
        </a>
        <a href="#addCustomerButton" className="w3-bar-item w3-button">
          Add Customer
        </a>
        <a href="#theWaitlist" className="w3-bar-item w3-button">
          The Waitlist
        </a>
        <a href="#clearWaitListButton" className="w3-bar-item w3-button">
          Clear Waitlist
        </a>
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="bgimg">
          <SidebarNav />
          <div id="pageContent">
            <DisplayHomepage />
            <br />
            <AddCustomer />
            <CustomerForm />
            <br />
            <DisplayCustomers />
            <br />
            <ClearWaitList />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const element = <App />;
ReactDOM.render(element, document.getElementById("contents"));
