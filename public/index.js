"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// Defaults that are to be removed for Tutorial 4
var currentDate = new Date(Date.now());
var waitlist = [{
  id: 1,
  customerName: "mickey mouse",
  phoneNumber: 84889687,
  createdTime: currentDate.toUTCString()
}, {
  id: 2,
  customerName: "cheshire cat",
  phoneNumber: 84172637,
  createdTime: currentDate.toUTCString()
}, {
  id: 3,
  customerName: "buzz lightyear",
  phoneNumber: 98472631,
  createdTime: currentDate.toUTCString()
}];
var pinkContainerClassName = "w3-container w3-padding-16 w3-pale-red w3-center w3-wide";
var genericButtonClassName = "w3-button w3-round w3-red w3-opacity w3-hover-opacity-off";
var deleteButtonClassName = "w3-button w3-round w3-grey w3-opacity w3-hover-opacity-off";

var AddCustomer = /*#__PURE__*/function (_React$Component) {
  _inherits(AddCustomer, _React$Component);

  var _super = _createSuper(AddCustomer);

  function AddCustomer() {
    _classCallCheck(this, AddCustomer);

    return _super.apply(this, arguments);
  }

  _createClass(AddCustomer, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "addCustomerButton"
      }, /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "Add a Customer"));
    }
  }]);

  return AddCustomer;
}(React.Component);

var CustomerForm = /*#__PURE__*/function (_React$Component2) {
  _inherits(CustomerForm, _React$Component2);

  var _super2 = _createSuper(CustomerForm);

  function CustomerForm() {
    _classCallCheck(this, CustomerForm);

    return _super2.apply(this, arguments);
  }

  _createClass(CustomerForm, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/React.createElement("label", {
        htmlFor: "custName"
      }, /*#__PURE__*/React.createElement("b", null, " Name:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custName",
        name: "custName"
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("label", {
        htmlFor: "custPhone"
      }, /*#__PURE__*/React.createElement("b", null, " Phone Number:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custPhone",
        name: "custPhone"
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "Add Customer to Waitlist")));
    }
  }]);

  return CustomerForm;
}(React.Component);

var DeleteCustomer = /*#__PURE__*/function (_React$Component3) {
  _inherits(DeleteCustomer, _React$Component3);

  var _super3 = _createSuper(DeleteCustomer);

  function DeleteCustomer() {
    _classCallCheck(this, DeleteCustomer);

    return _super3.apply(this, arguments);
  }

  _createClass(DeleteCustomer, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("button", {
        className: deleteButtonClassName
      }, /*#__PURE__*/React.createElement("i", {
        className: "fa fa-close"
      }));
    }
  }]);

  return DeleteCustomer;
}(React.Component);

var DisplayCustomers = /*#__PURE__*/function (_React$Component4) {
  _inherits(DisplayCustomers, _React$Component4);

  var _super4 = _createSuper(DisplayCustomers);

  function DisplayCustomers() {
    _classCallCheck(this, DisplayCustomers);

    return _super4.apply(this, arguments);
  }

  _createClass(DisplayCustomers, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("h3", null, "The Waitlist"), /*#__PURE__*/React.createElement(WaitListTable, null), /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "View Waitlist"));
    }
  }]);

  return DisplayCustomers;
}(React.Component);

var DisplayFreeSlots = /*#__PURE__*/function (_React$Component5) {
  _inherits(DisplayFreeSlots, _React$Component5);

  var _super5 = _createSuper(DisplayFreeSlots);

  function DisplayFreeSlots() {
    _classCallCheck(this, DisplayFreeSlots);

    return _super5.apply(this, arguments);
  }

  _createClass(DisplayFreeSlots, [{
    key: "render",
    value: function render() {
      var MAX_WAITING = 25;
      var waitlistNum = waitlist.length;
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, "Number of Free Slots: ", MAX_WAITING - waitlistNum);
    }
  }]);

  return DisplayFreeSlots;
}(React.Component);

var ClearWaitList = /*#__PURE__*/function (_React$Component6) {
  _inherits(ClearWaitList, _React$Component6);

  var _super6 = _createSuper(ClearWaitList);

  function ClearWaitList() {
    _classCallCheck(this, ClearWaitList);

    return _super6.apply(this, arguments);
  }

  _createClass(ClearWaitList, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "clearWaitListButton"
      }, /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "Clear Waitlist"));
    }
  }]);

  return ClearWaitList;
}(React.Component);

var DisplayHomepage = /*#__PURE__*/function (_React$Component7) {
  _inherits(DisplayHomepage, _React$Component7);

  var _super7 = _createSuper(DisplayHomepage);

  function DisplayHomepage() {
    _classCallCheck(this, DisplayHomepage);

    return _super7.apply(this, arguments);
  }

  _createClass(DisplayHomepage, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "homepage"
      }, /*#__PURE__*/React.createElement("div", {
        className: "w3-container w3-padding-16 w3-pale-blue w3-center w3-wide"
      }, /*#__PURE__*/React.createElement("h1", {
        className: "w3-text-grey"
      }, /*#__PURE__*/React.createElement("b", null, "Hotel California Waitlist System"))), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayFreeSlots, null));
    }
  }]);

  return DisplayHomepage;
}(React.Component);

var WaitListRow = /*#__PURE__*/function (_React$Component8) {
  _inherits(WaitListRow, _React$Component8);

  var _super8 = _createSuper(WaitListRow);

  function WaitListRow() {
    _classCallCheck(this, WaitListRow);

    return _super8.apply(this, arguments);
  }

  _createClass(WaitListRow, [{
    key: "render",
    value: function render() {
      var customerProp = this.props.customer;
      return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, customerProp.id), /*#__PURE__*/React.createElement("td", null, customerProp.customerName), /*#__PURE__*/React.createElement("td", null, customerProp.phoneNumber), /*#__PURE__*/React.createElement("td", null, customerProp.createdTime), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement(DeleteCustomer, null)));
    }
  }]);

  return WaitListRow;
}(React.Component);

var WaitListTable = /*#__PURE__*/function (_React$Component9) {
  _inherits(WaitListTable, _React$Component9);

  var _super9 = _createSuper(WaitListTable);

  function WaitListTable() {
    _classCallCheck(this, WaitListTable);

    return _super9.apply(this, arguments);
  }

  _createClass(WaitListTable, [{
    key: "render",
    value: function render() {
      var waitListRows = waitlist.map(function (customer) {
        return /*#__PURE__*/React.createElement(WaitListRow, {
          key: customer.id,
          customer: customer
        });
      });
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "theWaitlist"
      }, /*#__PURE__*/React.createElement("table", null, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null, "ID"), /*#__PURE__*/React.createElement("th", null, "Customer Name"), /*#__PURE__*/React.createElement("th", null, "Phone Number"), /*#__PURE__*/React.createElement("th", null, "Created Time"))), /*#__PURE__*/React.createElement("tbody", null, waitListRows)));
    }
  }]);

  return WaitListTable;
}(React.Component);

var SidebarNav = /*#__PURE__*/function (_React$Component10) {
  _inherits(SidebarNav, _React$Component10);

  var _super10 = _createSuper(SidebarNav);

  function SidebarNav() {
    _classCallCheck(this, SidebarNav);

    return _super10.apply(this, arguments);
  }

  _createClass(SidebarNav, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "sidebarNav",
        className: "w3-sidebar w3-light-grey w3-bar-block"
      }, /*#__PURE__*/React.createElement("h3", {
        className: "w3-bar-item"
      }, /*#__PURE__*/React.createElement("b", null, "\u2630 Menu")), /*#__PURE__*/React.createElement("a", {
        href: "#homepage",
        className: "w3-bar-item w3-button"
      }, "Homepage"), /*#__PURE__*/React.createElement("a", {
        href: "#addCustomerButton",
        className: "w3-bar-item w3-button"
      }, "Add Customer"), /*#__PURE__*/React.createElement("a", {
        href: "#theWaitlist",
        className: "w3-bar-item w3-button"
      }, "The Waitlist"), /*#__PURE__*/React.createElement("a", {
        href: "#clearWaitListButton",
        className: "w3-bar-item w3-button"
      }, "Clear Waitlist"));
    }
  }]);

  return SidebarNav;
}(React.Component);

var App = /*#__PURE__*/function (_React$Component11) {
  _inherits(App, _React$Component11);

  var _super11 = _createSuper(App);

  function App() {
    _classCallCheck(this, App);

    return _super11.apply(this, arguments);
  }

  _createClass(App, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "bgimg"
      }, /*#__PURE__*/React.createElement(SidebarNav, null), /*#__PURE__*/React.createElement("div", {
        id: "pageContent"
      }, /*#__PURE__*/React.createElement(DisplayHomepage, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(AddCustomer, null), /*#__PURE__*/React.createElement(CustomerForm, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayCustomers, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(ClearWaitList, null))));
    }
  }]);

  return App;
}(React.Component);

var element = /*#__PURE__*/React.createElement(App, null);
ReactDOM.render(element, document.getElementById("contents"));